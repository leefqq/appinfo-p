package com.appinfo.pojo;

import lombok.Data;

import java.util.Date;


@Data
public class AppVersion {
    private Long id;//bigint(30) NOT NULL AUTO_INCREMENT COMMENT '主键id',
    private Long appId;// bigint(30) DEFAULT NULL COMMENT 'appId（来源于：app_info表的主键id）',
    private String versionNo;// varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '版本号',
    private String versionInfo; //` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '版本介绍',
    private Long publishStatus; //` bigint(30) DEFAULT NULL COMMENT '发布状态（来源于：data_dictionary，1 不发布 2 已发布 3 预发布）',
    private String downloadLink; //` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '下载链接',
    private Double versionSize; //` decimal(20,2) DEFAULT NULL COMMENT '版本大小（单位：M）',
    private Long createdBy; //` bigint(30) DEFAULT NULL COMMENT '创建者（来源于dev_user开发者信息表的用户id）',
    private Date creationDate; //` datetime DEFAULT NULL COMMENT '创建时间',
    private Long modifyBy; //` bigint(30) DEFAULT NULL COMMENT '更新者（来源于dev_user开发者信息表的用户id）',
    private Date modifyDate; //` datetime DEFAULT NULL COMMENT '最新更新时间',
    private String apkLocPath; //` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'apk文件的服务器存储路径',
    private String apkFileName; //` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '上传的apk文件名称',
}
