package com.appinfo.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class AppInfo {
    private Long id;
    private String softwareName;
    private String APKName;
    private String supportROM;
    private String interfaceLanguage;
    private double softwareSize; //decimal(20,2) DEFAULT NULL COMMENT '软件大小（单位：M）',
    private Date updateDate;
    private Long devId;// '开发者id（来源于：dev_user表的开发者id）',
    private String appInfo;// 应用简介',
    private Long status; // '状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）',
    private Date onSaleDate ;//`  '上架时间',
    private Date offSaleDate; //`  '下架时间',
    private Long flatformId;//` '所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）',
    private Long categoryLevel3; //'所属三级分类（来源于：data_dictionary）',
    private Long downloads;//'下载量（单位：次）',
    private Long createdBy; //` bigint(30) DEFAULT NULL COMMENT '创建者（来源于dev_user开发者信息表的用户id）',
    private Date creationDate; //DEFAULT NULL COMMENT '创建时间',
    private Long modifyBy; //` bigint(30) DEFAULT NULL COMMENT '更新者（来源于dev_user开发者信息表的用户id）',
    private Date modifyDate;//` datetime DEFAULT NULL COMMENT '最新更新时间',
    private Long categoryLevel1;//` bigint(30) DEFAULT NULL COMMENT '所属一级分类（来源于：data_dictionary）',
    private Long categoryLevel2;//` bigint(30) DEFAULT NULL COMMENT '所属二级分类（来源于：data_dictionary）',
    private String logoPicPath;//` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'LOGO图片url路径',
    private String logoLocPath;//` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'LOGO图片的服务器存储路径',
    private Long versionId;//` bigint(30) DEFAULT NULL COMMENT '最新的版本id',


}
