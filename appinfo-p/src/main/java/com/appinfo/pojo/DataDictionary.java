package com.appinfo.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class DataDictionary {
    private Long id;
    private String typeCode;
    private String typeName;
    private Long valueId;
    private String valueName;
    private Long createdBy;
    private Date creationDate;
    private Long modifyBy;
    private Date modifyDate;
}
