package com.appinfo.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class AppCategory {
      private Long id;
      private String categoryCode;
      private String categoryName;
      private Long parentId;
      private Long createdB;
      private Date creationTime;
      private Long modifyBy;
      private Date modifyDate;
}
