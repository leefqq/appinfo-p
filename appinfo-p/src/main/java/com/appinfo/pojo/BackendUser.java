package com.appinfo.pojo;

import lombok.Data;

import java.sql.Date;

@Data
public class BackendUser {
    private Long id;
    private String userName;
    private String userCode;
    private Long userType;
    private Long createdBy;
    private Date creationDate;
    private Long modifyBy;
    private Date modifyDate;
    private String userPassword;
}
