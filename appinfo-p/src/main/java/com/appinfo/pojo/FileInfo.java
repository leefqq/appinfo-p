package com.appinfo.pojo;

import lombok.Data;

import java.beans.ConstructorProperties;

@Data
public class FileInfo {
    private String path;

    public FileInfo(String path) {
        this.path = path;
    }
}
