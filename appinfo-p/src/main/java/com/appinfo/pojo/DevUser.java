package com.appinfo.pojo;

import lombok.Data;
import lombok.ToString;

import java.sql.Date;

@Data
@ToString
public class DevUser {
    private Long id;
    private String devCode;
    private String devName;
    private String devPassword;
    private String devEmail;
    private String devInfo;
    private Long createdBy;
    private Date creationDate;
    private Long modifyBy;
    private Date modifyDate;

}
