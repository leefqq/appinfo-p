package com.appinfo.redis;

public class SubTokenKey extends BasePrefix{
    public static final int TOKEN_EXPIRE = 1000;


    public SubTokenKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }

    public static SubTokenKey subToken = new SubTokenKey(TOKEN_EXPIRE,"st");


    public static SubTokenKey getKey(int expireSeconds){
        return new SubTokenKey(expireSeconds,"st");
    }
}
