package com.appinfo.redis;

public class DevUserKey extends BasePrefix{
    public static final int TOKEN_EXPIRE = 3600*24 * 2;

    public DevUserKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }
    public static DevUserKey token = new DevUserKey(TOKEN_EXPIRE, "userTk");
}
