package com.appinfo.mapper;

import com.appinfo.pojo.AppVersion;
import com.appinfo.vo.AppVersionVo;
import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Mapper
public interface AppVersionMapper {

    @Select("select * from app_version where appId = #{id}")
    List<AppVersion> queryVersionListById(@Param("id") Long id);


    @Insert("insert into app_version(appId,versionNo,versionInfo,publishStatus,downloadLink,versionSize," +
            "createdBy,creationDate,modifyBy,modifyDate,apkLocPath,apkFileName) " +
            "value(#{appId},#{versionNo},#{versionInfo},#{publishStatus},#{downloadLink},#{versionSize},#{createdBy}," +
            "#{creationDate},#{modifyBy},#{modifyDate},#{apkLocPath},#{apkFileName})")
    @SelectKey(keyColumn = "id",keyProperty = "id",resultType = long.class,before = false,statement = "select last_insert_id()")
    Long saveAppVersion(AppVersion appVersion);

    @Select("select * from app_version where id = #{vid}")
    AppVersion queryVersionByVid(@Param("vid") Long vid);

    @Update("update app_version set versionSize = #{versionSize} ,versionInfo = #{versionInfo}," +
            "downloadLink=#{downloadLink},modifyBy=#{modifyBy},modifyDate=#{modifyDate} where id = #{id}")
    void updateVersion(AppVersion appVersion);

    @Update("update app_version set downloadLink = null , apkLocPath = null where id = #{id}")
    void delAppApkById(@Param("id") Long id);
}
