package com.appinfo.mapper;

import com.appinfo.pojo.AppInfo;
import com.appinfo.pojo.AppVersion;
import com.appinfo.provider.AppMapperProvider;
import com.appinfo.vo.AppInfoVo;
import com.appinfo.vo.Pages;
import org.apache.ibatis.annotations.*;


import java.util.List;

@Mapper
public interface AppMapper {

    @SelectProvider(type = AppMapperProvider.class,method = "queryAppListByQueryVo")
    List<AppInfo> selectAppInfoByQueryVo(Pages pages);


    @SelectProvider(type = AppMapperProvider.class,method = "countAppByQueryVo")
    int countAppListByQueryVo(Pages pages);

    @Select("select versionNo from app_version where appId = #{appId} and id = " +
            "(select max(id) from app_version where appId = #{appId})")
    String queryVersionNoByVId(@Param("appId") Long appId);


    @Select("select valueName from data_dictionary where valueId = #{valueId} and typeCode = #{typeCode}")
    String queryTypeNameByFId(@Param("valueId")Long valueId,@Param("typeCode")String typeCode);

    @Select("select categoryName from app_category where id = #{levelId}")
    String queryCategoryLevelNamesById(@Param("levelId") Long levelId);

    @Select("select * from app_info where APKName = #{apkName}")
    AppInfo queryAPKNameByName(@Param("apkName") String apkName);

    @Insert("insert into app_info (id,softwareName,APKName,supportROM,interfaceLanguage,softwareSize,updateDate,devId," +
            "appInfo,status,onSaleDate,offSaleDate,flatformId,categoryLevel3,downloads,createdBy," +
            "creationDate,modifyBy,modifyDate,categoryLevel1,categoryLevel2,logoPicPath,logoLocPath,versionId) " +
            "values(#{id},#{softwareName},#{APKName},#{supportROM},#{interfaceLanguage},#{softwareSize},#{updateDate}," +
            "#{devId},#{appInfo},#{status},#{onSaleDate},#{offSaleDate},#{flatformId},#{categoryLevel3}," +
            "#{downloads},#{createdBy},#{creationDate},#{modifyBy},#{modifyDate},#{categoryLevel1},#{categoryLevel2}," +
            "#{logoPicPath},#{logoLocPath},#{versionId})")
    @SelectKey(keyColumn = "id",keyProperty = "id",resultType = long.class,before = false,statement = "select last_insert_id()")
    long saveApp(AppInfo appInfo);

    @Select("select * from app_info where id = #{id}")
    AppInfo queryAppInfoById(@Param("id") Long id);

    @Update("update app_info set softwareName = #{softwareName},supportROM=#{supportROM},interfaceLanguage=#{interfaceLanguage}," +
            "softwareSize=#{softwareSize},downloads = #{downloads},flatformId = #{flatformId},categoryLevel1 = #{categoryLevel1}," +
            "categoryLevel2 = #{categoryLevel2},categoryLevel3 = #{categoryLevel3},appInfo = #{appInfo} ,modifyDate = #{modifyDate}," +
            "modifyBy = #{modifyBy} ,logoPicPath=#{logoPicPath},logoLocPath=#{logoLocPath} where id = #{id}")
    void updateAppInfoByVo(AppInfoVo appInfoVo);

    @Update("update app_info set versionId = #{vid} where id = #{appId}")
    void updateAppInfoVersionById(@Param("vid") Long vid,@Param("appId") Long appId);



    @Select("select * from app_info where id=#{appInfoId}")
    AppInfo checkAppInfoById(Long appInfoId);

    @Select("select * from app_version where appId=#{appInfoId}")
    List<AppVersion> checkVersionInfoById(@Param("appInfoId") long appInfoId);

    @Select("SELECT valueName FROM data_dictionary WHERE valueId=#{publicStatus} AND typecode=#{typeCode}")
    String querypublishStatusNameByStatus(@Param("publicStatus") Long publicStatus, @Param("typeCode") String typeCode);

    @Select("SELECT softwareName FROM app_info WHERE id=#{appId}")
    String queryappNameById(@Param("appId") Long appId);

    @Delete("DELETE app_info,app_version FROM app_info LEFT JOIN app_version ON app_info.id=app_version.appId WHERE app_info.id=#{appInfoId}")
    void delAppInfoById(Long appInfoId);

    @Select("select * from app_version where id = #{versionId}")
    AppVersion getAppVersionById(Long versionId);

    @Update("update app_info set status=#{status} where id=#{id}")
    boolean updateAppStatusById(@Param(value="status")Long status, @Param(value="id")Long id);

    @Update("update app_info set status=4 where id=#{id}")
    int onSale(Long appid);

    @Update("update app_info set status=5 where id=#{id}")
    int offSale(Long appid);

    @Update("update app_info set logoPicPath = null ,logoLocPath = null where id = #{id}")
    void delAppInfoLogo(@Param("id") Long id);
}
