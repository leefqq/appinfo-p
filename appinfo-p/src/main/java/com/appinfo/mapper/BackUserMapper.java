package com.appinfo.mapper;

import com.appinfo.pojo.BackendUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface BackUserMapper {

    @Select("select * from backend_user where userCode=#{userCode} and userPassword=#{userPassword}")
    BackendUser loginBackEndUser(BackendUser backendUser);
}
