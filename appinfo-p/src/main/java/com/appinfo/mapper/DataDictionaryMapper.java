package com.appinfo.mapper;

import com.appinfo.pojo.DataDictionary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DataDictionaryMapper {
    @Select("select * from data_dictionary where typeCode = #{appStatus}")
    List<DataDictionary> queryAppDataDicList(@Param("appStatus") String appStatus);

    @Select("select valueName from data_dictionary where typeCode = #{typeCode} and valueId = #{publishStatus}")
    String queryAppDataDicName(@Param("typeCode") String typeCode, @Param("publishStatus") Long publishStatus);
}
