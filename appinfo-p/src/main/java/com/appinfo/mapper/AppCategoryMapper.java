package com.appinfo.mapper;

import com.appinfo.pojo.AppCategory;
import com.appinfo.provider.AppCategoryProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@Mapper
public interface AppCategoryMapper {
    @SelectProvider(type = AppCategoryProvider.class,method = "queryAppCategoryLevel")
    List<AppCategory> queryCategoryLevel(@Param("arg0") List<Long> pids);

    @Select("select * from app_category where parentId = #{pid}")
    List<AppCategory> queryCategoryAllLevel(@Param("pid")Long pid);

    @Select("select * from app_category where parentId is NULL;")
    List<AppCategory> queryCategoryLevel1();


}
