package com.appinfo.mapper;


import com.appinfo.pojo.DevUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


@Mapper
public interface DevUserMapper {

    @Select("select * from dev_user where devCode = #{devCode}")
    DevUser queryUser(@Param("devCode") String devCode);
}
