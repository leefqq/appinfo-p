package com.appinfo.controller;

import com.appinfo.pojo.AppInfo;
import com.appinfo.pojo.DataDictionary;
import com.appinfo.pojo.DevUser;
import com.appinfo.result.Result;
import com.appinfo.service.AppService;
import com.appinfo.service.DataDictionaryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.List;

@Controller
@RequestMapping("/dev/flatform/app")
public class DataDictionaryController {
    @Autowired
    private DataDictionaryService dataDictionaryService;
    @Autowired
    private AppService appService;


    @RequestMapping("/datadictionarylist.json")
    @ResponseBody
    public List<DataDictionary> queryDataDicListByTCode(@RequestParam("tcode")String tcode){
        return dataDictionaryService.queryAppDataDicList(tcode);
    }

    @RequestMapping("/apkexist.json")
    @ResponseBody
    public AppInfo queryApkName(AppInfo appInfo){
        AppInfo nappInfo = appService.queryAPKNameByName(appInfo.getAPKName());
        return nappInfo;
    }







}
