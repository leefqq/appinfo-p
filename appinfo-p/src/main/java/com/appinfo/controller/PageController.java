package com.appinfo.controller;

import com.appinfo.pojo.DevUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@Controller
public class PageController {
    /**
     * 首页
     * @return
     */
    @RequestMapping("/index")
    public ModelAndView toIndex(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        return mv;
    }

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }


    @RequestMapping("/backend/login")
    public String toManagerLogin(){
        return "backendlogin";
    }

    /**
     * 开发者登陆页面
     * @return
     */
    @RequestMapping("/dev/login")
    public String toDevLogin(){
        return "devlogin";
    }

    @RequestMapping("/loginError")
    public String error(){
        return "/error";
    }

    @RequestMapping("/developer/main")
    public String toDevMain(Model model, DevUser devUser){
        model.addAttribute("user",devUser);
        return "developer/main";
    }
}
