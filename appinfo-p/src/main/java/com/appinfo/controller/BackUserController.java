package com.appinfo.controller;

import com.appinfo.pojo.BackendUser;

import com.appinfo.service.BackUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class BackUserController {

    @Autowired
    private BackUserService backUserService;

    /**
     * 后台管理登录
     * @param backendUser
     * @return
     */
    @RequestMapping(value = "/backend/doLogin",method = RequestMethod.POST)
    public String backEndLogin(BackendUser backendUser, HttpServletRequest request) {

        HttpSession session = request.getSession();

        BackendUser loginBackEndUser = backUserService.loginBackEndUser(backendUser);

        if (loginBackEndUser != null) {
            session.setAttribute("userSession", loginBackEndUser);
            return "/backend/main";
        } else {
            return "/backend/login";
        }
    }


    /**
     * 后台管理用户注销
     *
     * @return
     */
    @RequestMapping(value = "/backend/logout")
    public String backEndLoginOut(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("userSession");
        return "backendlogin";
    }
}
