package com.appinfo.controller;


import com.appinfo.pojo.AppCategory;
import com.appinfo.pojo.AppInfo;
import com.appinfo.pojo.DataDictionary;
import com.appinfo.pojo.DevUser;
import com.appinfo.service.AppCategoryService;
import com.appinfo.service.AppService;
import com.appinfo.service.BackAppService;
import com.appinfo.service.DataDictionaryService;
import com.appinfo.vo.AppInfoVo;
import com.appinfo.vo.AppVersionVo;
import com.appinfo.vo.Pages;
import com.appinfo.vo.QueryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/backend")
public class BackAppController {

    private static final String APP_STATUS = "APP_STATUS"; //状态
    private static final String APP_FLATFORM = "APP_FLATFORM"; //平台

    @Autowired
    private AppService appService;

    @Autowired
    private BackAppService backAppService;

    @Autowired
    private DataDictionaryService dataDictionaryService;

    @Autowired
    private AppCategoryService appCategoryService;




    /**
     * 查看app列表
     * @param request
     * @param devUser
     * @param pages
     * @param model
     * @param queryVo
     * @return
     */
    @RequestMapping("/list")
    public String showBackAppList(HttpServletRequest request, DevUser devUser, Pages pages ,
                                  Model model, QueryVo queryVo) {

        pages = appService.queryAppInfoPage(pages, queryVo);
        List<AppInfoVo> appInfoVos = pages.getRows();
        model.addAttribute("pages", pages);

        //状态栏
        List<DataDictionary> appDic = dataDictionaryService.queryAppDataDicList(APP_STATUS);
        List<DataDictionary> flatFormDic = dataDictionaryService.queryAppDataDicList(APP_FLATFORM);
        //一、二、三级分类列表
        List<AppCategory> categoryLevel1List = appCategoryService.queryCategoryLevel(null);

        List<Long> CL1Pids = categoryLevel1List.stream().map(appCategory -> {
            Long cl1Pid = appCategory.getId();
            return cl1Pid;
        }).collect(Collectors.toList());


        List<AppCategory> categoryLevel2List = appCategoryService.queryCategoryLevel(CL1Pids);

        List<Long> CL2Pids = categoryLevel2List.stream().map(appCategory -> {
            Long cl2Pid = appCategory.getId();
            return cl2Pid;
        }).collect(Collectors.toList());

        List<AppCategory> categoryLevel3List = appCategoryService.queryCategoryLevel(CL2Pids);

        //写入下拉列表
        model.addAttribute("querySoftwareName",pages.getQuerySoftwareName());
        model.addAttribute("queryStatus",pages.getQueryStatus());
        model.addAttribute("queryFlatformId",pages.getQueryFlatformId());
        model.addAttribute("queryCategoryLevel1",pages.getQueryCategoryLevel1());
        model.addAttribute("queryCategoryLevel2",pages.getQueryCategoryLevel2());
        model.addAttribute("queryCategoryLevel3",pages.getQueryCategoryLevel3());

        //属性
        model.addAttribute("categoryLevel1List",categoryLevel1List);
        model.addAttribute("categoryLevel2List",categoryLevel2List);
        model.addAttribute("categoryLevel3List",categoryLevel3List);
        model.addAttribute("statusList",appDic);
        model.addAttribute("flatFormList", flatFormDic);
        model.addAttribute("appInfoList",appInfoVos); //信息列表

        return "backend/applist";

    }


    /**
     * 查看APP信息
     * @param appInfoId
     * @param versionId
     * @param model
     * @return
     */
    @RequestMapping("/check")
    public String findAndCheckApp(@RequestParam(value = "aid",required = false) Long appInfoId,
                                  @RequestParam(value = "vid",required = false) Long versionId,
                                  Model model) {

        AppInfoVo appInfo = backAppService.getAppInfoById(appInfoId);
        AppVersionVo appVersionVo = backAppService.getAppVersionById(versionId);

        model.addAttribute("appInfo",appInfo);
        model.addAttribute("appVersion",appVersionVo);

        return "backend/appcheck";

    }


    /**
     * 审核APP
     * @param appInfo
     * @return
     */
    @RequestMapping("/checkAndSaveApp")
    public String checkAndSaveApp(AppInfo appInfo) {

        boolean isSuccess = backAppService.updateAppStatusById(appInfo.getStatus(),appInfo.getId());
        if (isSuccess) {
            return "redirect:/backend/list";
        } else {
            return "backend/appcheck";
        }

    }


}
