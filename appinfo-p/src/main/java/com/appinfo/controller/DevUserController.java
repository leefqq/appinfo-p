package com.appinfo.controller;

import com.appinfo.exception.GlobalException;
import com.appinfo.pojo.DevUser;
import com.appinfo.result.CodeMsg;
import com.appinfo.result.Result;
import com.appinfo.service.DevUserService;
import com.appinfo.vo.LoginVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class DevUserController {
    private static Logger log = LoggerFactory.getLogger(DevUserController.class);

    @Autowired
    private DevUserService devUserService;


    @RequestMapping(value = "/dev/doLogin",method = RequestMethod.POST)
    @ResponseBody
    public Result<Boolean> devLogin(LoginVo loginVo, HttpServletResponse response){
        log.info("Login_info:"+loginVo.toString());
        devUserService.devLogin(response,loginVo);
        return Result.success(true);
    }

    @RequestMapping(value = "/dev/logout")
    public String devLogout(DevUser devUser , HttpServletRequest request,HttpServletRequest response){
        log.info("注销用户信息："+devUser.toString());
        devUserService.devLogout(devUser, request);
        return "devlogin";
    }
}
