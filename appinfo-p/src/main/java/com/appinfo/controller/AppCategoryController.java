package com.appinfo.controller;

import com.appinfo.pojo.AppCategory;
import com.appinfo.service.AppCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AppCategoryController {

    @Autowired
    private AppCategoryService appCategoryService;


    @RequestMapping("/dev/flatform/app/categorylevellist.json")
    public List<AppCategory> queryCategoryLevel(@RequestParam(value = "pid",required = false)Long pid){
        List<AppCategory> categories = appCategoryService.queryCategoryAllLevel(pid);
        return categories;
    }

}
