package com.appinfo.provider;

import com.appinfo.vo.Pages;
import com.appinfo.vo.QueryVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.SQL;

public class AppMapperProvider {
    private final String TB_APPINFO = "app_info";

    /**
     * 通过pages查找app列表
     * @param pages
     * @return
     */
    public String queryAppListByQueryVo(Pages pages){
        SQL sql = new SQL().SELECT("*").FROM(TB_APPINFO);
        sql.WHERE("1 = 1");
        SQL nSQL = appTermByQueryVo(pages, sql);
        String fSql = nSQL.toString()+" limit #{startRow},#{size}";
        return fSql;
    }

    /**
     * 通过queryvo统计appinfo
     * @param pages
     * @return
     */
    public String countAppByQueryVo(Pages pages){
        SQL sql = new SQL().SELECT("count(1)").FROM(TB_APPINFO);
        sql.WHERE("1 = 1");
        SQL nSQL = appTermByQueryVo(pages,sql);
        return nSQL.toString();
    }


    /**
     * 根据条件查找app
     * @param pages
     * @param sql
     * @return
     */
    private SQL appTermByQueryVo(Pages pages,SQL sql){
        String softwareName = pages.getQuerySoftwareName();
        if(StringUtils.isNotBlank(softwareName)){
            //sql.WHERE("softwareName like '%#{querySoftwareName}%'");
           sql.AND().WHERE("softwareName like concat('%',#{querySoftwareName},'%')");
        }
        if(pages.getQueryStatus()!=null){
            sql.AND().WHERE("status = #{queryStatus}");
        }
        if(pages.getQueryFlatformId()!=null) {
            sql.AND().WHERE("flatformId = #{queryFlatformId}");
        }
        if(pages.getQueryCategoryLevel1()!=null) {
            sql.AND().WHERE("categoryLevel1 = #{queryCategoryLevel1}");
        }
        if(pages.getQueryCategoryLevel2()!=null) {
            sql.AND().WHERE("categoryLevel2 = #{queryCategoryLevel2}");
        }
        if(pages.getQueryCategoryLevel3()!=null) {
            sql.AND().WHERE("categoryLevel3 = #{queryCategoryLevel3}");
        }
        return sql;
    }
}
