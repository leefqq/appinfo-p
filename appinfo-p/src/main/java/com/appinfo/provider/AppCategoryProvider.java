package com.appinfo.provider;

import org.apache.ibatis.jdbc.SQL;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AppCategoryProvider {
    private final String TB_APPCAT = "app_category";

    public String queryAppCategoryLevel(List<Integer> pids){

        SQL sql = new SQL().SELECT("*").FROM(TB_APPCAT);
        sql.WHERE("1 = 1");
        StringBuffer queryId = new StringBuffer(" and parentId in (");
        for (Integer pid = 0;pid < pids.size()-1;pid++){
            queryId.append(pids.get(pid)+",");
            /*if(pid!=null && pids.iterator().hasNext()) {
                queryId.append(pid+",");
            }
            if(pid != null && !pids.iterator().hasNext()){
                queryId.append(pid+")");
            }*/
        }
        queryId.append(pids.get(pids.size()-1)+")");
        String nSql = sql.toString()+queryId;
        return nSql;
    }

    @Test
    public void test1(){
        List<Integer> pids = new ArrayList<>();
        pids.add(1);
        pids.add(2);
        pids.add(3);
        String s = queryAppCategoryLevel(pids);
        System.out.println(s);
    }
}
