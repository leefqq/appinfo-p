package com.appinfo.result;

public class CodeMsg {
    private int code;
    private String msg;

    public CodeMsg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static CodeMsg BINDERROR = new CodeMsg(500501,"绑定参数异常：%s");
    public static CodeMsg SERVER_ERROR = new CodeMsg(500502,"服务器异常");
    public static CodeMsg USEREMPTY = new CodeMsg(500503,"用户不存在！");
    public static CodeMsg ISNOTLOGIN = new CodeMsg(500504,"访问错误，请先登录！");
    public static CodeMsg PASSWORDERROR = new CodeMsg(500504,"用户名或密码不正确！");



    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    //自定义参数
    public CodeMsg fillArgs(Object...args){
        int code = this.code;
        String msg = String.format(this.msg,args);
        return new CodeMsg(code,msg);
    }


}
