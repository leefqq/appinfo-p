package com.appinfo.service;


import com.appinfo.pojo.DevUser;
import com.appinfo.vo.LoginVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface DevUserService {
    DevUser devLogin(HttpServletResponse response, LoginVo loginVo);

    DevUser getByToken(HttpServletResponse response, String token);

    void devLogout(DevUser devUser, HttpServletRequest request);
}
