package com.appinfo.service;

import com.appinfo.pojo.AppVersion;
import com.appinfo.pojo.DevUser;
import com.appinfo.vo.AppVersionVo;

import java.util.List;

public interface AppVersionService {
    List<AppVersionVo> queryVersionListById(Long id);

    void saveAppVersion(AppVersion appVersion, DevUser devUser);

    AppVersion queryVersionByVid(Long vid);

    void updateVersion(AppVersion appVersion,DevUser devUser);

    void delAppApkById(Long id);
}
