package com.appinfo.service;

import com.appinfo.exception.GlobalException;
import com.appinfo.mapper.DevUserMapper;
import com.appinfo.pojo.DevUser;
import com.appinfo.redis.DevUserKey;
import com.appinfo.redis.RedisService;
import com.appinfo.result.CodeMsg;
import com.appinfo.utils.CheckObjectIsNull;
import com.appinfo.utils.UUIDUtil;
import com.appinfo.vo.LoginVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class DevUserServiceimpl implements DevUserService {
    public static final String COOKIE_NAME_TOKEN = "token";

    @Autowired
    private DevUserMapper devUserMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 登录
     * @param response
     * @param loginVo
     * @return
     */
    @Override
    public DevUser devLogin(HttpServletResponse response, LoginVo loginVo) {
        if(loginVo==null){
            throw new GlobalException(CodeMsg.SERVER_ERROR);
        }
        String devCode = loginVo.getDevCode();
        String devPassword = loginVo.getDevPassword();
        DevUser user = devUserMapper.queryUser(devCode);

        if(user==null){
            throw new GlobalException(CodeMsg.USEREMPTY);
        }
        if(!devPassword.equals(user.getDevPassword())){
            throw new GlobalException(CodeMsg.PASSWORDERROR);
        }

        String token = UUIDUtil.uuid();
        addCookie(response,token,user);
        return user;
    }

    @Override
    public DevUser getByToken(HttpServletResponse response, String token) {
        if(StringUtils.isEmpty(token)) {
            return null;
        }
        DevUser user = redisService.get(DevUserKey.token, token, DevUser.class);
        //延长有效期
        if(user != null) {
            addCookie(response, token, user);
        }
        return user;
    }

    /**
     * 注销
     * @param devUser
     * @param request
     * @return
     */
    @Override
    public void devLogout(DevUser devUser, HttpServletRequest request) {
        String cookieValue = getCookieValue(request, COOKIE_NAME_TOKEN);
        String paramToken = request.getParameter(COOKIE_NAME_TOKEN);
        String token = cookieValue==null?paramToken:cookieValue;
        if(token == null){
            return;
        }
        redisService.del(DevUserKey.token,token);
    }

    //添加用户信息到cookie
    private void addCookie(HttpServletResponse response, String token, DevUser user) {
        redisService.set(DevUserKey.token, token, user);
        Cookie cookie = new Cookie(COOKIE_NAME_TOKEN, token);
        cookie.setMaxAge(DevUserKey.token.expireSeconds());
        cookie.setPath("/");
        response.addCookie(cookie);

    }

    private String getCookieValue(HttpServletRequest request, String cookiName) {
        Cookie[]  cookies = request.getCookies();
        for(Cookie cookie : cookies) {
            if(cookie.getName().equals(cookiName)) {
                return cookie.getValue();
            }
        }
        return null;
    }

}
