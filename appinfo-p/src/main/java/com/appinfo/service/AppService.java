package com.appinfo.service;

import com.appinfo.pojo.AppInfo;
import com.appinfo.pojo.AppVersion;
import com.appinfo.pojo.DevUser;
import com.appinfo.vo.AppInfoVo;
import com.appinfo.vo.AppVersionVo;
import com.appinfo.vo.Pages;
import com.appinfo.vo.QueryVo;

import java.util.List;

public interface AppService {
    Pages<AppInfoVo> queryAppInfoPage(Pages pages,QueryVo queryVo);

    AppInfo queryAPKNameByName(String apkName);

    void saveApp(AppInfo appInfo, DevUser devUser);

    AppInfoVo queryAppInfoById(Long id);

    void updateAppInfoByVo(AppInfoVo appInfoVo,DevUser devUser);

    String querySoftwareNameById(Long id);

    void updateAppInfoVersionById(Long vid,Long appId);

    AppInfoVo checkAppInfoById(long appInfoId);

    List<AppVersionVo> checkVersionInfoById(long appInfoId);

    void delAppInfoById(Long appInfoId);

    boolean updateSaleStatusByAppId(Long appid);


    Boolean DelFile(Long id, String flag);
}
