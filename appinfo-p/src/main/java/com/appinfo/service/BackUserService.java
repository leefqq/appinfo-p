package com.appinfo.service;

import com.appinfo.pojo.BackendUser;

public interface BackUserService {

    BackendUser loginBackEndUser(BackendUser backendUser);
}
