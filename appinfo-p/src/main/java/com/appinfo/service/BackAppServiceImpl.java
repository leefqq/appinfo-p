package com.appinfo.service;

import com.appinfo.mapper.AppMapper;
import com.appinfo.pojo.AppInfo;
import com.appinfo.pojo.AppVersion;
import com.appinfo.vo.AppInfoVo;
import com.appinfo.vo.AppVersionVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BackAppServiceImpl implements BackAppService {

    //平台码
    private static final String FLATFORMCODE = "APP_FLATFORM";
    //app状态码
    private static final String STATUSCODE = "APP_STATUS";
    //app发布状态
    private static final String PUBLISHSTATUSCODE = "PUBLISH_STATUS";

    @Autowired
    private AppMapper appMapper;


    /**
     * 查询app信息
     * @param appInfoId
     * @return
     */
    @Override
    public AppInfoVo getAppInfoById(Long appInfoId) {
        AppInfo appInfo = appMapper.checkAppInfoById(appInfoId);

        AppInfoVo appInfoVo = new AppInfoVo();

        //分类名称
        String categoryLevel1Name = queryCategoryLevelNamesById(appInfo.getCategoryLevel1());
        String categoryLevel2Name = queryCategoryLevelNamesById(appInfo.getCategoryLevel2());
        String categoryLevel3Name = queryCategoryLevelNamesById(appInfo.getCategoryLevel3());
        //状态名称
        String statusName = queryTypeNameByFId(appInfo.getStatus(),STATUSCODE);
        //平台名称
        String flatformName = queryTypeNameByFId(appInfo.getFlatformId(),FLATFORMCODE);
        //版本号
        String versionNo = queryVersionNoByVId(appInfo.getId());

        appInfoVo.setCategoryLevel1Name(categoryLevel1Name);
        appInfoVo.setCategoryLevel2Name(categoryLevel2Name);
        appInfoVo.setCategoryLevel3Name(categoryLevel3Name);
        appInfoVo.setFlatformName(flatformName);
        appInfoVo.setStatusName(statusName);
        appInfoVo.setVersionNo(versionNo);

        //转换
        BeanUtils.copyProperties(appInfo,appInfoVo );

        return appInfoVo;
    }


    /**
     * 查询app版本信息
     * @param versionId
     * @return
     */
    @Override
    public AppVersionVo getAppVersionById(Long versionId) {
        AppVersion appVersion = appMapper.getAppVersionById(versionId);

        AppVersionVo appVersionVo =  new AppVersionVo();

        String appName = queryappNameById(appVersion.getAppId());
        String publishStatusName = querypublishStatusNameByStatus(appVersion.getPublishStatus(), PUBLISHSTATUSCODE);

        appVersionVo.setAppName(appName);
        appVersionVo.setPublishStatusName(publishStatusName);

        BeanUtils.copyProperties(appVersion,appVersionVo );

        return appVersionVo;
    }

    @Override
    public boolean updateAppStatusById(Long status, Long id) {

        return appMapper.updateAppStatusById(status, id);

    }


    /**
     * 查询最新版本号
     * @param appId
     * @return
     */
    private String queryVersionNoByVId(Long appId) {
        String versionNo = appMapper.queryVersionNoByVId(appId);
        return versionNo;
    }

    /**
     * 查询类别--状态名
     * @param valueId
     * @param typeCode
     * @return
     */
    private String queryTypeNameByFId(Long valueId, String typeCode) {
        String typeName = appMapper.queryTypeNameByFId(valueId,typeCode);
        return typeName;
    }


    private String queryCategoryLevelNamesById(Long levelId) {
        String levelName = appMapper.queryCategoryLevelNamesById(levelId);
        return levelName;
    }

    private String querypublishStatusNameByStatus(Long publicStatus, String typeCode) {
        String publishStatusName = appMapper.querypublishStatusNameByStatus(publicStatus,typeCode);
        return publishStatusName;
    }

    private String queryappNameById(Long appId) {
        String appName = appMapper.queryappNameById(appId);
        return appName;
    }
}
