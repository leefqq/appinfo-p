package com.appinfo.service;

import com.appinfo.mapper.AppCategoryMapper;
import com.appinfo.pojo.AppCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppCategoryServiceImpl implements AppCategoryService{
    @Autowired
    private AppCategoryMapper appCategoryMapper;


    @Override
    public List<AppCategory> queryCategoryLevel(List<Long> pids) {
        List<AppCategory> queryCategoryLevel = null;

        if(pids == null){
            queryCategoryLevel = appCategoryMapper.queryCategoryLevel1();
        }else {
            queryCategoryLevel = (appCategoryMapper.queryCategoryLevel(pids));
        }
        return queryCategoryLevel;
    }

    @Override
    public List<AppCategory> queryCategoryAllLevel(Long pid) {
        List<AppCategory> categories = null;
        if(pid != null){
            categories = appCategoryMapper.queryCategoryAllLevel(pid);
        }else {
            categories = queryCategoryLevel(null);
        }

        return categories;
    }


}
