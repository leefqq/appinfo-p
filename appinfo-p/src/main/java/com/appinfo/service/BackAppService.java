package com.appinfo.service;

import com.appinfo.vo.AppInfoVo;
import com.appinfo.vo.AppVersionVo;
import org.apache.ibatis.annotations.Param;

public interface BackAppService {

    AppInfoVo getAppInfoById(Long appInfoId);

    AppVersionVo getAppVersionById(Long versionId);

    boolean updateAppStatusById(@Param(value = "status") Long status, @Param(value = "id") Long id);
}
