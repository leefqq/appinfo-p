package com.appinfo.service;

import com.appinfo.pojo.DataDictionary;

import java.util.List;

public interface DataDictionaryService {
    List<DataDictionary> queryAppDataDicList(String appStatus);

    String queryAppDataDicName(String statuscode, Long publishStatus);
}
