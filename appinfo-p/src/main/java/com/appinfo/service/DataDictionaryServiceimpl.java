package com.appinfo.service;

import com.appinfo.mapper.DataDictionaryMapper;
import com.appinfo.pojo.DataDictionary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataDictionaryServiceimpl implements DataDictionaryService{

    @Autowired
    private DataDictionaryMapper dataDictionaryMapper;

    @Override
    public List<DataDictionary> queryAppDataDicList(String appStatus) {
        List<DataDictionary> appDic = dataDictionaryMapper.queryAppDataDicList(appStatus);
        return appDic;
    }

    @Override
    public String queryAppDataDicName(String statuscode, Long publishStatus) {
        String dataDicName = dataDictionaryMapper.queryAppDataDicName(statuscode,publishStatus);
        return dataDicName;
    }
}
