package com.appinfo.service;

import com.appinfo.mapper.BackUserMapper;
import com.appinfo.pojo.BackendUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BackUserServiceImpl implements BackUserService {

    @Autowired
    private BackUserMapper backUserMapper;

    /**
     * 登录后台管理
     * @param backendUser
     * @return
     */
    @Override
    public BackendUser loginBackEndUser(BackendUser backendUser) {
        return backUserMapper.loginBackEndUser(backendUser);
    }
}
