package com.appinfo.service;

import com.appinfo.pojo.AppCategory;

import java.util.List;

public interface AppCategoryService {
    List<AppCategory> queryCategoryLevel(List<Long> pids);

    List<AppCategory> queryCategoryAllLevel(Long pid);
}
