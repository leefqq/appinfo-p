package com.appinfo.vo;

import com.appinfo.pojo.AppCategory;
import com.appinfo.pojo.DataDictionary;
import lombok.Data;

import java.util.List;

@Data
public class QueryVo {
    private DataDictionary dataDictionaries;
    private List<String> statusList;
    private List<String> flatFormList;
    private AppCategory appCategory;
}
