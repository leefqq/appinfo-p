package com.appinfo.vo;

import javax.validation.constraints.NotNull;

public class LoginVo {
    @NotNull
    private String devCode;
    @NotNull
    private String devPassword;

    public String getDevCode() {
        return devCode;
    }

    public void setDevCode(String devCode) {
        this.devCode = devCode;
    }

    public String getDevPassword() {
        return devPassword;
    }

    public void setDevPassword(String devPassword) {
        this.devPassword = devPassword;
    }

    @Override
    public String toString() {
        return "LoginVo{" +
                "devCode='" + devCode + '\'' +
                ", devPassword='" + devPassword + '\'' +
                '}';
    }
}
