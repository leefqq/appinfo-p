package com.appinfo.vo;

import com.appinfo.pojo.AppVersion;
import lombok.Data;

@Data
public class AppVersionVo extends AppVersion {
    private String appName;
    private String publishStatusName;
}
