package com.appinfo.vo;

import com.appinfo.pojo.AppInfo;
import lombok.Data;

@Data
public class AppInfoVo extends AppInfo {
    private String flatformName; //平台名字
    private String categoryLevel1Name;
    private String categoryLevel2Name;
    private String categoryLevel3Name;
    private String statusName; //
    private String versionNo; //版本号
}
