package com.appinfo.vo;

import lombok.Data;

import java.util.List;

@Data
public class Pages<T> {
    //页对象数据
    private Integer totalCount;
    private Integer totalPageCount;
    private Integer currentPageNo = 0;
    private Integer size;
    private List<T> rows;
    private Integer pageIndex = 1;
    private Integer startRow = 0; //开始行
    //查询数据
    private String querySoftwareName;
    private Integer queryStatus;
    private Integer queryFlatformId;
    private Integer queryCategoryLevel1;
    private Integer queryCategoryLevel2;
    private Integer queryCategoryLevel3;



}
