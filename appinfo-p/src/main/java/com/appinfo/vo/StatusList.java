package com.appinfo.vo;

import com.appinfo.pojo.DataDictionary;
import lombok.Data;

@Data
public class StatusList {
    private DataDictionary dataDictionaries;
    private final String queryStatus = "APP_STATUS";
}
