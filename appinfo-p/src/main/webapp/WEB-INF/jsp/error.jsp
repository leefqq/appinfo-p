<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

    <title>后台管理系统</title>

    <!-- Bootstrap -->
    <link type="text/css" href="${pageContext.request.contextPath }/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link type="text/css" href="${pageContext.request.contextPath }/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link type="text/css" href="${pageContext.request.contextPath }/css/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link type="text/css" href="${pageContext.request.contextPath }/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
<div class="login_wrapper">
    <h1>APP信息管理平台</h1>
    <div>
        <h1>${msg}</h1>
    </div>

</div>
</body>
</html>