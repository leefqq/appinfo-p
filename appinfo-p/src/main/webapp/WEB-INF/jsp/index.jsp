<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

    <title>后台管理系统</title>

    <!-- Bootstrap -->
    <link type="text/css" href="${pageContext.request.contextPath }/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link type="text/css" href="${pageContext.request.contextPath }/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link type="text/css" href="${pageContext.request.contextPath }/css/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link type="text/css" href="${pageContext.request.contextPath }/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
<div class="login_wrapper">
    <h1>APP信息管理平台</h1>
    <div>
        <a href="backend/login" class="btn btn-link">后台管理系统 入口</a>
    </div>
    <div>
        <a href="dev/login" class="btn btn-link">开发者平台 入口</a>
    </div>
</div>
</body>
</html>