<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>APP开发者平台</title>

    <!-- jquery -->
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.min.js"></script>
    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/bootstrap/css/bootstrap.min.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath }/bootstrap/js/bootstrap.min.js"></script>

    <!-- layer -->
    <script type="text/javascript" src="${pageContext.request.contextPath }/layer/layer.js"></script>
    <!-- md5.js -->
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/md5.min.js"></script>
    <!-- common.js -->
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/common.js"></script>


    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath }/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="${pageContext.request.contextPath }/css/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="${pageContext.request.contextPath }/css/custom.min.css" rel="stylesheet">
  </head>

  <%--<body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="loginForm">
              <h1>APP开发者平台</h1>
              <div>
                <input id="devCode" name = "devCode" class="form-control" type="text" placeholder="用户名" required="true"  minlength="5" maxlength="11" />
              </div>
              <div>
                <input id="devPassword" name="devPassword" class="form-control" type="password"  placeholder="密码" required="true" minlength="6" maxlength="16" />
              </div>
              <span>${error }</span>
              <div>
              	<button type="submit" class="btn btn-success" onclick="doLogin()">登     录</button>
              	<button type="reset" class="btn btn-default" >重　填</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div>
                  <p>©2016 All Rights Reserved. </p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>--%>
  <body>
  <form align="center" id="loginForm" method="post">
    <h1 align="center">APP开发者平台</h1>
    <div class="form-group" align="center">
      <div class="row">
        <div class="col-md-4"></div>

        <label class="form-label col-xs-1">请输入用户名</label>
        <div class="col-xs-3">
          <input id="devCode" name = "devCode" class="form-control" type="text" placeholder="用户名" required="true"  minlength="5" maxlength="11" />
        </div>

        <div class="col-md-1">
        </div>
      </div>
    </div>


    <div class="form-group">
      <div class="row" align="center">

        <div class="col-md-4"></div>
        <label class="form-label  col-xs-1">请输入密码</label>
        <div class="col-xs-3">
          <input id="devPassword" name="devPassword" class="form-control" type="password"  placeholder="密码" required="true" minlength="6" maxlength="16" />
        </div>

        <div class="col-md-1"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
    </div>


    <div class="row" align="center">
      <div class="col-md-4"></div>
      <div class="col-md-2">
        <button class="btn btn-primary btn-block" type="reset" onclick="reset()">重置</button>
      </div>
      <div class="col-md-2">
        <button class="btn btn-primary btn-block" type="button" onclick="doLogin()">登录</button>
      </div>
      <div class="col-md-4"></div>
    </div>

  </form>
  </body>
  <script>
    function login(){
      $("#loginForm").validate({
        submitHandler:function(form){
          doLogin();
        }
      });
    }
    function doLogin() {
      g_showLoading();

      var devCode = $("#devCode").val();
      var devPassword = $("#devPassword").val();
      $.ajax({
        url: "/dev/doLogin",
        type: "POST",
        data:{
          devCode :devCode,
          devPassword:devPassword
        },
        success:function (data) {
          layer.closeAll();
          if(data.code == 0){
            alert("登录成功！");
            //layer.msg("成功！");
            window.location.href="/developer/main";
          }else {
            alert(data.msg);
          }
        },
        error:function(){
          alert("登录失败，密码或账号出错！");
          layer.closeAll();
        }
      });
    }
  </script>
</html>